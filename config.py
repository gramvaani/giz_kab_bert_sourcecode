#===================================================================#
#                   General Configuration                           #
#===================================================================#
"""

KAB : 'srhr_bert_dataset_with_themes (1).xlsx' 

"""
# For train and test collumn, choose one among 'Caller query transcription'/ 'Relevant Topic' / 'STT Transcript'
EXCEL_FILE_PATH = ['srhr_bert_dataset_with_themes (1).xlsx']
DATA = 'KAB'
TEST_COLUMN = 'Caller query transcription'
TRAIN_COLUMN = 'Sanitized Question' 
MAX_LEN = 256
LEARNING_RATE_1 = 2e-6
LEARNING_RATE_2 = 2e-5
EPOCH_NUM = 8
BATCH_SIZE = 8
TEST_BATCH_SIZE = 50
MODEL_NAME = 'BERT'

#===================================================================#
#                   Excel Sheet Configuration                       #
#===================================================================#

COLUMN_NAMES = {
    'Theme' : 'Theme',
    'Sanitized Question' : 'Sanitized Question',
    'Caller query transcription' : 'Caller query transcription'
}

