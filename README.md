# GIZ KAB BERT QnA Model

Our code can be used for identifying the query in an QnA database, which is most similar to the input test query. We hope our contribution helps future researchers in providing a headstart on experiments. Please add your data inside the data directory and modify the code for reading them appropriately.

## Overview

This repository contains the implementation of a machine learning model in the `main.ipynb` file.

### Model

A Transformer-based architecture, specifically BERT (bert-base-multilingual-cased), is employed using TensorFlow. The model is trained on question pairs for a similarity classification task. Positive samples consist of similar question pairs, while negative samples involve random question pairs. The pretrained multilingual BERT model from HuggingFace is utilized to obtain embeddings for each input.Eventually, as a result the model generates similarity scores for question pairs in the test set.

## Getting Started

### USAGE

data_creation.ipynb : generates the test & train csv files along with negative samples, for further usage in main.ipynb. This will split the all_data in 80:20 ratio based on caller query transcriptions and then generate 1.75 times of negative samples for the train file for improved model training, and for test file, it will map each of the 20% caller query transcription with all the sanitized questions. 

main.ipynb : The notebook contains the complete implementation of the model.Users can customize the notebook for their specific data by following the instructions/comments.

All the notebooks describe their workflow in detail.The notebooks download this repository, and then use the data present in the data folder for training, testing and evaluation.
